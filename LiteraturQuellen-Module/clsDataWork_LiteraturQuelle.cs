﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using Ontology_Module;
using Structure_Module;
using OntologyClasses.BaseClasses;

namespace LiteraturQuellen_Module
{
    
    public class clsDataWork_LiteraturQuelle
    {
        private clsLocalConfig objLocalConfig;

        private OntologyModDBConnector objDBLevel_LiteraturQuellen;

        private OntologyModDBConnector objDBLevel_Filter1;
        private OntologyModDBConnector objDBLevel_Filter2;
        private OntologyModDBConnector objDBLevel_Filter3;
        private OntologyModDBConnector objDBLevel_Filter4;
        private OntologyModDBConnector objDBLevel_RefItems;

        public clsOntologyItem OItem_Result_LiteraturQuellen { get; private set; }

        private List<clsOntologyItem> FilterQuellen;

        public SortableBindingList<clsLiteraturQuelle> OList_LiteraturQuellen { get; set; }

        public void GetData_LiteraturQuellen()
        {
            OItem_Result_LiteraturQuellen = objLocalConfig.Globals.LState_Nothing.Clone();

            OItem_Result_LiteraturQuellen = getLiteraturQuellenOfRefItems();

            var objORL_Literaturquellen = new List<clsObjectRel> {
                new clsObjectRel {ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID, 
                    ID_Parent_Object = objLocalConfig.OItem_type_audio_quelle.GUID },
                new clsObjectRel {ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID, 
                    ID_Parent_Object = objLocalConfig.OItem_type_bild_quelle.GUID },
                new clsObjectRel {ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID, 
                    ID_Parent_Object = objLocalConfig.OItem_type_buch_quellenangabe.GUID },
                new clsObjectRel {ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID, 
                    ID_Parent_Object = objLocalConfig.OItem_type_email_quelle.GUID },
                new clsObjectRel {ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID, 
                    ID_Parent_Object = objLocalConfig.OItem_type_internet_quellenangabe.GUID },
                new clsObjectRel {ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID, 
                    ID_Parent_Object = objLocalConfig.OItem_type_video_quelle.GUID },
                new clsObjectRel {ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID, 
                    ID_Parent_Object = objLocalConfig.OItem_type_zeitungsquelle.GUID } };

            var objOItem_Result = objDBLevel_LiteraturQuellen.GetDataObjectRel(objORL_Literaturquellen, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (FilterQuellen != null)
                {
                    OList_LiteraturQuellen = new SortableBindingList<clsLiteraturQuelle>(
                    from litQuelle in objDBLevel_LiteraturQuellen.ObjectRels.Select(p => new clsLiteraturQuelle
                        {
                            ID_LiteraturQuelle = p.ID_Other,
                            Name_LiteraturQuelle = p.Name_Other,
                            ID_Quelle = p.ID_Object,
                            Name_Quelle = p.Name_Object,
                            ID_Class_Quelle = p.ID_Parent_Object,
                            Name_Class_Quelle = p.Name_Parent_Object
                        })
                        join filterQuelle in FilterQuellen on litQuelle.ID_LiteraturQuelle equals filterQuelle.GUID
                        select litQuelle);
                }
                else
                {
                    OList_LiteraturQuellen = new SortableBindingList<clsLiteraturQuelle>(objDBLevel_LiteraturQuellen.ObjectRels.Select(p => new clsLiteraturQuelle
                        {
                            ID_LiteraturQuelle = p.ID_Other,
                            Name_LiteraturQuelle = p.Name_Other,
                            ID_Quelle = p.ID_Object,
                            Name_Quelle = p.Name_Object,
                            ID_Class_Quelle = p.ID_Parent_Object,
                            Name_Class_Quelle = p.Name_Parent_Object
                        }));
                }
                

                OItem_Result_LiteraturQuellen = objOItem_Result;
            }
            else
            {
                OItem_Result_LiteraturQuellen = objLocalConfig.Globals.LState_Error.Clone();
            }
        }

        public clsOntologyItem getLiteraturQuellenOfRefItems()
        {
            var result = objLocalConfig.Globals.LState_Success.Clone();

            FilterQuellen = null;
            if (objLocalConfig.OItem_RefItems != null && objLocalConfig.OItem_RefItems.Count == 1 && objLocalConfig.OItem_RefItems.First().GUID_Parent == objLocalConfig.OItem_type_literarische_quelle.GUID)
            {
                FilterQuellen = new List<clsOntologyItem> { new clsOntologyItem 
                {
                    GUID = objLocalConfig.OItem_RefItems.First().GUID,
                    Name = objLocalConfig.OItem_RefItems.First().Name,
                    GUID_Parent = objLocalConfig.OItem_RefItems.First().GUID_Parent,
                    Type = objLocalConfig.Globals.Type_Object
                }};
            }
            else if (objLocalConfig.OItem_RefItems != null && objLocalConfig.OItem_RefItems.Count == 1 && 
                objLocalConfig.QuellKlassenListe.Any(cls => cls.GUID == objLocalConfig.OItem_RefItems.First().GUID_Parent))
            {
                var searchLiteraturQuelle = new List<clsObjectRel> {new clsObjectRel
                {
                    ID_Object = objLocalConfig.OItem_RefItems.First().GUID,
                    ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID
                }};

                result = objDBLevel_RefItems.GetDataObjectRel(searchLiteraturQuelle, doIds: false);
                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    FilterQuellen = objDBLevel_RefItems.ObjectRels.Select(quellen => new clsOntologyItem
                    {
                        GUID = quellen.ID_Other,
                        Name = quellen.Name_Other,
                        GUID_Parent = quellen.ID_Parent_Other,
                        Type = quellen.Ontology
                    }).ToList();
                }

            }
            else if (objLocalConfig.OItem_RefItems != null && objLocalConfig.OItem_RefItems.Any())
            {
                var searchLiteraturQuellen_LR = objLocalConfig.OItem_RefItems.Select(refI => new clsObjectRel
                {
                    ID_Object = refI.GUID,
                    ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID
                }).ToList();

                var searchLiteraturQuellen_RL = objLocalConfig.OItem_RefItems.Select(refI => new clsObjectRel
                {
                    ID_Other = refI.GUID,
                    ID_Parent_Object = objLocalConfig.OItem_type_literarische_quelle.GUID
                }).ToList();

                result = objDBLevel_RefItems.GetDataObjectRel(searchLiteraturQuellen_LR, doIds: false);

                if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    FilterQuellen = objDBLevel_RefItems.ObjectRels.Select(quellen => new clsOntologyItem
                    {
                        GUID = quellen.ID_Other,
                        Name = quellen.Name_Other,
                        GUID_Parent = quellen.ID_Parent_Other,
                        Type = quellen.Ontology
                    }).ToList();

                    result = objDBLevel_RefItems.GetDataObjectRel(searchLiteraturQuellen_RL, doIds: false);

                    if (result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        FilterQuellen.AddRange(objDBLevel_RefItems.ObjectRels.Select(quellen => new clsOntologyItem
                        {
                            GUID = quellen.ID_Object,
                            Name = quellen.Name_Object,
                            GUID_Parent = quellen.ID_Parent_Object,
                            Type = objLocalConfig.Globals.Type_Object
                        }));
                    }
                }
            }
            else if (objLocalConfig.OItem_RefItems != null && !objLocalConfig.OItem_RefItems.Any())
            {
                FilterQuellen = new List<clsOntologyItem>();
            }
            return result;
        }

        public List<clsOntologyItem> getFilterQuellen(clsOntologyItem OItem_Filter)
        {
            List<clsOntologyItem> OList_LiteraturQuellen = new List<clsOntologyItem>();

            if (OItem_Filter.GUID_Parent == objLocalConfig.OItem_type_literatur.GUID)
            {
                var oList_Obj1 = new List<clsOntologyItem> {new clsOntologyItem { GUID = OItem_Filter.GUID,
                    Name = OItem_Filter.Name,
                    GUID_Parent = OItem_Filter.GUID_Parent} };

                var objOItem_Result = objDBLevel_Filter1.GetDataObjects(oList_Obj1);
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {

                    var oList_Rel1 = objDBLevel_Filter1.Objects1.Select(p => new clsObjectRel { ID_Other = p.GUID,
                        ID_RelationType = objLocalConfig.OItem_relationtype_belonging_source.GUID,
                        ID_Parent_Object = objLocalConfig.OItem_type_buch_quellenangabe.GUID,
                        ID_Object = OItem_Filter.GUID,
                        Name_Object = OItem_Filter.Name}).ToList();
                    if (oList_Rel1.Any())
                    {
                        objOItem_Result = objDBLevel_Filter2.GetDataObjectRel(oList_Rel1);

                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            var oList_Rel2 = objDBLevel_Filter2.ObjectRelsId.Select(p => new clsObjectRel
                            {
                                ID_Object = p.ID_Object,
                                ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID,
                                ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID
                            }).ToList();

                            objOItem_Result = objDBLevel_Filter3.GetDataObjectRel(oList_Rel2);

                            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                            {
                                OList_LiteraturQuellen = objDBLevel_Filter3.ObjectRelsId.Select(p => new clsOntologyItem
                                {
                                    GUID = p.ID_Other,
                                    GUID_Parent = p.ID_Parent_Other,
                                    Type = objLocalConfig.Globals.Type_Object
                                }).ToList();


                            }
                            else
                            {
                                OList_LiteraturQuellen = null;
                            }
                        }
                        else
                        {
                            OList_LiteraturQuellen.Clear();
                        }
                    }
                    else
                    {
                        OList_LiteraturQuellen.Clear();
                    }

                    

                }
                else
                {
                    OList_LiteraturQuellen = null;
                }
                
            }
            else if (OItem_Filter.GUID_Parent == objLocalConfig.OItem_attribute_seite.GUID)
            {
                var oList_Rel1 = new List<clsObjectAtt> {new clsObjectAtt {ID_AttributeType = objLocalConfig.OItem_attribute_seite.GUID, 
                    ID_Class = objLocalConfig.OItem_type_buch_quellenangabe.GUID,
                    Val_String = OItem_Filter.Name}};

                var objOItem_Result = objDBLevel_Filter1.GetDataObjectAtt(oList_Rel1);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    var oList_Rel2 = objDBLevel_Filter1.ObjAttsId.Select(p => new clsObjectRel
                    {
                        ID_Object = p.ID_Object,
                        ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID,
                        ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID
                    }).ToList();

                    if (oList_Rel2.Any())
                    {
                        objOItem_Result = objDBLevel_Filter2.GetDataObjectRel(oList_Rel2);

                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            OList_LiteraturQuellen = objDBLevel_Filter2.ObjectRelsId.Select(p => new clsOntologyItem
                            {
                                GUID = p.ID_Other,
                                GUID_Parent = p.ID_Parent_Other,
                                Type = objLocalConfig.Globals.Type_Object
                            }).ToList();


                        }
                        else
                        {
                            OList_LiteraturQuellen = null;
                        }
                    }
                    else
                    {
                        OList_LiteraturQuellen.Clear();
                    }

                    
                }
                else
                {
                    OList_LiteraturQuellen = null;
                }
            }
            else if (OItem_Filter.GUID_Parent == objLocalConfig.OItem_type_url.GUID)
            {
                var oList_Obj1 = new List<clsOntologyItem> {new clsOntologyItem { GUID = OItem_Filter.GUID,
                    Name = OItem_Filter.Name,
                    GUID_Parent = OItem_Filter.GUID_Parent} };

                var objOItem_Result = objDBLevel_Filter1.GetDataObjects(oList_Obj1);
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {

                    var oList_Rel1 = objDBLevel_Filter1.Objects1.Select(p => new clsObjectRel
                    {
                        ID_Other = p.GUID,
                        ID_RelationType = objLocalConfig.OItem_relationtype_belonging_source.GUID,
                        ID_Parent_Object = objLocalConfig.OItem_type_internet_quellenangabe.GUID
                    }).ToList();

                    if (oList_Rel1.Any())
                    {
                        objOItem_Result = objDBLevel_Filter2.GetDataObjectRel(oList_Rel1);

                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            var oList_Rel2 = objDBLevel_Filter2.ObjectRelsId.Select(p => new clsObjectRel
                            {
                                ID_Object = p.ID_Object,
                                ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID,
                                ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID
                            }).ToList();

                            if (oList_Rel2.Any())
                            {
                                objOItem_Result = objDBLevel_Filter3.GetDataObjectRel(oList_Rel2);

                                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                {
                                    OList_LiteraturQuellen = objDBLevel_Filter3.ObjectRelsId.Select(p => new clsOntologyItem
                                    {
                                        GUID = p.ID_Other,
                                        GUID_Parent = p.ID_Parent_Other,
                                        Type = objLocalConfig.Globals.Type_Object
                                    }).ToList();


                                }
                                else
                                {
                                    OList_LiteraturQuellen = null;
                                }
                            }

                        }
                        else
                        {
                            OList_LiteraturQuellen = null;
                        }
                    }
                    else
                    {
                        OList_LiteraturQuellen.Clear();
                    }

                }
                else
                {
                    OList_LiteraturQuellen = null;
                }
            }
            else if (OItem_Filter.GUID_Parent == objLocalConfig.OItem_class_media_item.GUID)
            {
                var oList_Obj1 = new List<clsOntologyItem> {new clsOntologyItem { GUID = OItem_Filter.GUID,
                    Name = OItem_Filter.Name,
                    GUID_Parent = OItem_Filter.GUID_Parent} };

                var objOItem_Result = objDBLevel_Filter1.GetDataObjects(oList_Obj1);

                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    var oList_Rel1 = objDBLevel_Filter1.Objects1.Select(p => new clsObjectRel
                    {
                        ID_Object = p.GUID,
                        ID_RelationType = objLocalConfig.OItem_relationtype_belongsto.GUID,
                        ID_Parent_Other = objLocalConfig.OItem_type_video.GUID
                    }).ToList();

                    if (oList_Rel1.Any())
                    {
                        objOItem_Result = objDBLevel_Filter2.GetDataObjectRel(oList_Rel1);

                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            var oList_Rel2 = objDBLevel_Filter2.ObjectRelsId.Select(p => new clsObjectRel
                            {
                                ID_Other = p.ID_Other,
                                ID_RelationType = objLocalConfig.OItem_relationtype_belonging.GUID,
                                ID_Parent_Object = objLocalConfig.OItem_type_video_quelle.GUID
                            }).ToList();

                            if (oList_Rel2.Any())
                            {
                                objOItem_Result = objDBLevel_Filter3.GetDataObjectRel(oList_Rel2);

                                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                {
                                    //Video-quelle
                                    var oList_Rel3 = objDBLevel_Filter3.ObjectRelsId.Select(p => new clsObjectRel
                                    {
                                        ID_Object = p.ID_Object,
                                        ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID,
                                        ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID
                                    }).ToList();

                                    if (oList_Rel3.Any())
                                    {
                                        objOItem_Result = objDBLevel_Filter4.GetDataObjectRel(oList_Rel3);
                                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                        {
                                            OList_LiteraturQuellen = objDBLevel_Filter4.ObjectRelsId.Select(p => new clsOntologyItem
                                            {
                                                GUID = p.ID_Other,
                                                GUID_Parent = p.ID_Parent_Other,
                                                Type = objLocalConfig.Globals.Type_Object
                                            }).ToList();
                                        }
                                        else
                                        {
                                            OList_LiteraturQuellen = null;
                                        }
                                    }




                                }
                                else
                                {
                                    OList_LiteraturQuellen = null;
                                }
                            }

                        }
                        else
                        {
                            OList_LiteraturQuellen = null;
                        }
                    }
                    else
                    {
                        OList_LiteraturQuellen.Clear();
                    }
                    
                }
                else
                {
                    OList_LiteraturQuellen = null;
                }

                if (OList_LiteraturQuellen != null)
                {
                    if (!OList_LiteraturQuellen.Any())
                    {
                        var oList_Rel2 = objDBLevel_Filter1.Objects1.Select(p => new clsObjectRel
                        {
                            ID_Object = p.GUID,
                            ID_RelationType = objLocalConfig.OItem_relationtype_belongsto.GUID,
                            ID_Parent_Object = objLocalConfig.OItem_type_audio_quelle.GUID
                        }).ToList();

                        objOItem_Result = objDBLevel_Filter2.GetDataObjectRel(oList_Rel2);
                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            var oList_Rel3 = objDBLevel_Filter2.ObjectRelsId.Select(p => new clsObjectRel
                            {
                                ID_Object = p.ID_Object,
                                ID_RelationType = objLocalConfig.OItem_relationtype_issubordinated.GUID,
                                ID_Parent_Other = objLocalConfig.OItem_type_literarische_quelle.GUID
                            }).ToList();


                            objOItem_Result = objDBLevel_Filter3.GetDataObjectRel(oList_Rel3);
                            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                            {
                                OList_LiteraturQuellen = objDBLevel_Filter3.ObjectRelsId.Select(p => new clsOntologyItem
                                {
                                    GUID = p.ID_Other,
                                    GUID_Parent = p.ID_Parent_Other,
                                    Type = objLocalConfig.Globals.Type_Object
                                }).ToList();
                            }
                            else
                            {
                                OList_LiteraturQuellen = null;
                            }
                        }
                        else
                        {
                            OList_LiteraturQuellen = null;
                        }
                    }
                    else
                    {
                        OList_LiteraturQuellen.Clear();
                    }
                }

            }
            else if (OItem_Filter.GUID_Parent == objLocalConfig.OItem_class_images__graphic_.GUID)
            {
                
            }
            else if (OItem_Filter.GUID_Parent == objLocalConfig.OItem_class_e_mail.GUID)
            {

            }
            else if (OItem_Filter.GUID_Parent == objLocalConfig.OItem_class_zeitschrift.GUID)
            {

            }
            else if (OItem_Filter.GUID_Parent == objLocalConfig.OItem_type_zeitschriftenausgabe.GUID)
            {

            }

            return OList_LiteraturQuellen;
        }

        public clsDataWork_LiteraturQuelle(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;
            Initialize();
        }

        private void Initialize()
        {
            objDBLevel_LiteraturQuellen = new OntologyModDBConnector(objLocalConfig.Globals);

            objDBLevel_Filter1 = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Filter2 = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Filter3 = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Filter4 = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RefItems = new OntologyModDBConnector(objLocalConfig.Globals);

            OItem_Result_LiteraturQuellen = objLocalConfig.Globals.LState_Nothing.Clone();
        }
    }
}
